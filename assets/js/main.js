function get_age(dateString = '1990-06-28')
{
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
    {
        age--;
    }
    return age;
}

function format_date_br(date){
    var data = new Date(date+'T00:00:00'),
        dia  = data.getDate().toString().padStart(2, '0'),
        mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
        ano  = data.getFullYear();
    return dia+"/"+mes+"/"+ano;
}

function convertFormToJSON(form) {
    const array = $(form).serializeArray(); // Encodes the set of form elements as an array of names and values.
    const json = {};
    $.each(array, function () {
        json[this.name] = this.value || "";
    });
    return json;
}

function setUser() {
    var users = JSON.parse(window.localStorage.getItem('user')) ?? [];
    var data = convertFormToJSON($('form#create-user'));
    var state_name = $('[name="state"] option:selected').data('uf');
    var city_name = $('[name="city"]').val();
    data.state = state_name;
    data.city = city_name;
    if (data.index !== undefined){
        users[data.index] = data;
        alert('Usuário editado com sucesso!')
        $('#edit').remove()
    }else{
        users.push(data);
        alert('Novo usuário criado com sucesso!')
    }
    $('#create-user')[0].reset();
    $('#submitButton').empty()
    $('#submitButton').append(`<img src="assets/imgs/arrow.svg" alt=""> &nbsp;&nbsp;&nbsp;&nbsp;Incluir`)
    window.localStorage.setItem('user', JSON.stringify(users));
    var new_index = users.length - 1;
    loadList()
}

function removeUser(index){
    var users = window.localStorage.getItem('user');
    users = JSON.parse(users)
    users.splice(index, 1);
    window.localStorage.setItem('user', JSON.stringify(users));
    loadList()
}

function loadList(){
    var users = window.localStorage.getItem('user');
    $('#list-users tbody').empty();
    $.each(JSON.parse(users), function (index, item){
        $('#list-users tbody').append(`
        <tr>
            <td>${item.fullname}</td>
            <td>${item.cpf}</td>
            <td>${format_date_br(item.date_birth)}</td>
            <td>${get_age(item.date_birth)}</td>
            <td>${item.state}</td>
            <td>${item.city}</td>
            <td class="text-center"><img src="./assets/imgs/pencil.svg" data-edit="${index}" alt=""></td>
            <td class="text-center"><img src="./assets/imgs/trash.svg" data-remove="${index}" alt=""></td>
        </tr>
        `)
    })
}

function setEdit(index) {
    var users = window.localStorage.getItem('user');
    $('[name="state"] option').attr('selected', false)
    $('[name="city"] option').attr('selected', false)
    users = JSON.parse(users)
    user = users[index];
    $('[name="fullname"]').val(user.fullname)
    $('[name="cpf"]').val(user.cpf)
    $('[name="date_birth"]').val(user.date_birth)
    $('[name="state"] option[data-uf="'+user.state+'"]').attr('selected', true).trigger('change')
    setTimeout(function (){
        $('[name="city"] option[value="'+user.city+'"]').attr('selected', true)
    },800)
    $('#create-user').prepend(`<input type="hidden" id="edit" name="index" value="${index}">`)
    $('#submitButton').empty()
    $('#submitButton').append(`<img src="assets/imgs/arrow.svg" alt=""> &nbsp;&nbsp;&nbsp;&nbsp;Editar`)
    loadList()
}



$(document).ready(function (){
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('form').submit(function (e){
        e.preventDefault();
        setUser();
    })
    $.get('http://www.geonames.org/childrenJSON?geonameId=3469034')
        .done(function (data){
            data = data.geonames
            $('[name="state"] :not(:first-child)').remove()
            $.each(data, function (i, item){
                $('[name="state"]').append(`<option data-uf="${item.adminCodes1.ISO3166_2}" value="${item.geonameId}"> ${item.name}</option>`)
            })
        })



    $('[name="state"]').change(function (){
        $.get('http://www.geonames.org/childrenJSON?geonameId='+$(this).val())
            .done(function (data){
                data = data.geonames
                $('[name="city"] :not(:first-child)').remove()
                $.each(data, function (i, item){
                    $('[name="city"]').append(`<option value="${item.name}"> ${item.name}</option>`)
                })
            })
    })

    $(document).on('click', '[data-remove]',function (){
        removeUser($(this).data('remove'))
    })
    $(document).on('click', '[data-edit]',function (){
        setEdit($(this).data('edit'));
    })
    loadList()
})
